﻿using System;

namespace AutoServiceClient
{
    public sealed class Program
    {
        public static int Main(string[] args)
        {
            AutoServiceTestClient client = new AutoServiceTestClient("AutoServiceEndpoint");
            client.GetVehicleByVin("ggg");
            client.Close();
            return 0;
        }
    }
}
