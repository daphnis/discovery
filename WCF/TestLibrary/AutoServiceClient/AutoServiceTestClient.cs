﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using AutoServiceContract;

namespace AutoServiceClient
{
    public class AutoServiceTestClient : ClientBase<IAutoService>, IAutoService
    {
        public AutoServiceTestClient(string endpointName)
            : base(endpointName)
        {
        }

        public AutoServiceTestClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        public Vehicle GetVehicleByVin(string vin)
        {
            return Channel.GetVehicleByVin(vin);
        }
    }
}
