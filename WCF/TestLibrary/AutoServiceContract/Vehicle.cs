﻿using System;
using System.Runtime.Serialization;

namespace AutoServiceContract
{
    [DataContract]
    public class Vehicle
    {
        string vin = String.Empty;

        [DataMember]
        public string Vin
        {
            get
            {
                return vin;
            }
            set
            {
                vin = value;
            }
        }
    }
}
