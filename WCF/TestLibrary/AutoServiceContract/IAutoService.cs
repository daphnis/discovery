﻿using System;
using System.ServiceModel;

namespace AutoServiceContract
{
    [ServiceContract]
    public interface IAutoService
    {
        [OperationContract]
        Vehicle GetVehicleByVin(string vin);
    }
}
