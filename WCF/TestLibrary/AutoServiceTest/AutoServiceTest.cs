﻿using System;
using System.Diagnostics;
using NUnit.Framework;

namespace AutoServiceTest
{
    [TestFixture]
    public class AutoServiceTest
    {
        [Test]
        public void TestAutoServiceMethod()
        {
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.FileName = @"c:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\WcfSvcHost.exe";
            process.StartInfo.Arguments = @"/service:""C:\Users\Yama\Google Диск\Work\WCF\TestLibrary\TestLibrary\bin\Debug\TestLibrary.dll"" /config:""C:\Users\Yama\Google Диск\Work\WCF\TestLibrary\TestLibrary\bin\Debug\TestLibrary.dll.config""";
            process.Start();

            string clientPathName = @"c:/tmp/AutoServiceClient.exe"; // @"C:\Users\Yama\""Google Диск""\Work\WCF\TestLibrary\TestLibrary\bin\Debug\AutoServiceClient.exe";
            Process clientProcess = new Process();
            clientProcess.StartInfo.UseShellExecute = true;
            clientProcess.StartInfo.RedirectStandardOutput = false;
            clientProcess.StartInfo.FileName = clientPathName;
            try
            {
                clientProcess.Start();
                clientProcess.WaitForExit();
                string output = clientProcess.StandardOutput.ReadToEnd();
                Assert.IsFalse(clientProcess.ExitCode == 0, output);
            }
            finally
            {
                process.Kill();
            }
        }
    }
}
