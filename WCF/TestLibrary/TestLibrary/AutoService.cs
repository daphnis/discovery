﻿using System;
using AutoServiceContract;

namespace TestLibrary
{
    public class AutoService : IAutoService
    {
        public Vehicle GetVehicleByVin(string vin)
        {
            return new Vehicle() {Vin = vin};
        }
    }
}
