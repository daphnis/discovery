﻿using System;
using System.Threading;

namespace PrivateCall
{
    public static class Program
    {
        static void Main(string[] args)
        {
            DeadLock lock1 = new DeadLock();
            DeadLock2 lock2 = new DeadLock2();
            lock1.Other = lock2;
            lock2.Other = lock1;
            lock1.Demo();
            lock2.Demo();
        }
    }

    internal class DeadLock
    {
        public DeadLock Other { get; set; }

        public void Demo()
        {
            Thread.Sleep(1000);
            Other.Hello();
        }

        private void Hello()
        {
            Console.WriteLine("Hello!");
        }
    }

    internal class DeadLock2
    {
        public DeadLock Other { get; set; }

        public void Demo()
        {
            Thread.Sleep(1000);
            Other.Hello();
        }

        private void Hello()
        {
            Console.WriteLine("Hello!");
        }
    }
}
