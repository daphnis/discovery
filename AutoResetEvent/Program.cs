﻿using System;
using System.Threading;

namespace AutoResetEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            Tester t = new Tester();
            t.Start();
            Thread.Sleep(100);
            t.Stop();
            Console.Read();
        }
    }
}
