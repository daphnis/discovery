﻿using System;
using System.Threading;

namespace AutoResetEvent
{
    /// <summary>
    /// A and B should be in ABAB order and A should be always typed first
    /// </summary>
    internal class Tester
    {
        private EventWaitHandle ev1 = new System.Threading.AutoResetEvent(false);
        private EventWaitHandle ev2 = new System.Threading.AutoResetEvent(false);
        private Thread thread1;
        private Thread thread2;
        private bool initialized = false;
        private object locker = new object();

        public void Start()
        {
            thread1 = new Thread(Worker1);
            ev1.Set();
            thread1.Start();
            thread2 = new Thread(Worker2);
            thread2.Start();
        }

        public void Stop()
        {
            thread1.Abort();
            thread2.Abort();
        }

        private void Worker1()
        {
            while (true)
            {
                ev1.WaitOne();
                Console.WriteLine("A");
                ev2.Set();
            }
        }

        private void Worker2()
        {
            while (true)
            {
                ev2.WaitOne();
                Console.WriteLine("B");
                ev1.Set();
            }
        }
    }
}
